import os
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()

install_requires = [
	'Django>=2.1',
]

tests_require = [
	'factory-boy>=2.11.1',
	'freezegun>=0.3.10',
]

setup(
	name='django-officehours',
	version='0.0.0',
	packages=['officehours'],
	description='A LINE OF DESCRIPTION HERE',
	long_description=README,
	author='David G Rosenberg',
	author_email='dgrosen.dev@gmail.com',
	url='https://gitlab.com/targendaz2/django-officehours',
	license='',
	install_requires=install_requires,
)
