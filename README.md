# django-officehours

Provides views and forms for setting office hours in a Django project. This is particularly useful for apps which are dependent on a potentially changing daily schedule.